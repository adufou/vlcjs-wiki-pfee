- Start with basics undertanding of Electron.

- Make a simple hello_world in Electron with node and run it.

- Make another hello_world in C, compile it with Emscripten and then run it in Electron.

- Run vlc.js in Electron as it is.

- In order to make the most modulable app, we must remove the entrypoint of vlc.js (main.c) by changing how we compile the project to only have a lib of vlc functions such as play() or create_media(). Those functions should be called directly from .js files.

- Finally, on top of the canvas playing the video, we should create a new and cleaner overlay.

Draft
