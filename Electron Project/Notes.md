# VLC.js General Notes

vlc/ <- sources

libvlc.wasm -> need IMPORT, EXPORT, MEMORY


The module -> variable which contains the preRun, postRun, functions, the canvas, the status, ... etc
js file that allows HTML to prepare the runtime

var Module = {
    canvas:
    
    ...

    onRunTimeInitialized
}

the experimental.js -> auto generated file, with emcc ... -o experimental.js

# TODO

In VLC's CI, update to generate the artifacts
Look at the paths at the end of create_main.sh to know what we will need / where the artifacts are (maybe then move them cleanly)

# For Integration with Electron

Compile VLC.js, take artifacts (ex libvlc.wasm)

If we take the Nightlies, we have the contribs in lib. This is the longest stuff to compile.
Missing libvlcore, libvlc, and modules.


The purpose of the main is to execute something in itself

Except that the purpose for Electron is to NOT have an entry point (no main.c), but to let the user load, play media etc...
Having an entry point leaves no choice to the user, so the goal is to compile (emcc) all the libraries (contrib + libvlccore + libvlc + modules), to bind all the symbols together vlc-modules.bc, and make it accessible for JS.
Instead of the main.c (entry point), we could give it a list of functions that we are likely to use, so that they are accessible in the JS, because we have to declare them somewhere. part, so that they are not deleted because of the optimization of the compilation (for reasons of space, because it is heavy in GB to host everything...).
