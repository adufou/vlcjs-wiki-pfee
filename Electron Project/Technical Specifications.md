# Introduction

This document is a technical specification document.

It contains the rules that must be satisfied for the VLC.js and VLC.js Electron project:

- The versions of the different technologies that will be structuring

- Compatibility with the different software that we will use for the development of the project

- Compatibility with the different hardwares, or platforms on which our project will be implemented

In addition, this document contains a technical framework that contains any material exclusions, or specific rules on the final product.

# Technologies

## Technologies used

The project based on VLC.js and Electron, the technologies used will be:

- Electron.
- Node.js.
- VLC.js.
- Chromium.
- Docker / Docker Compose.
- VLC.js extensions/dependencies.

## Technology Versions

This list is temporary and subject to change.

- VLC.js is in development version, which does not allow to precisely indicate its version.
- Electron: 15.x.x.
- Node: 16.x.x.
- Chromium: 94.x.x.
- Emcc/Em++: 2.0.x.
- Clang: 13.0.x.

## Reminder on Web assembly technology

The project is mainly based on recent advances in terms of web application development, and in particular WebAssembly technology.

From this extremely strong need to allow the execution of high-performance code within browsers was created WebAssembly, or Wasm, is a technology for integrating low-level code into a web browser.

It was designed to facilitate the integration of applications designed in compiled language (such as C, C++ and Rust), therefore native to the operating system and the architecture of the target machine, in a transparent environment. -platform (web), without having to rewrite them from scratch.

The C/C++/Rust code of an existing application can be converted into binary via a specific toolchain (Emscripten29), which compiles the native code into the Wasm module.

This binary is platform independent, and theoretically browser independent.

The virtual machine integrated into the browsers can thus load and execute the Wasm binary directly within its javascript environment, allowing much higher performance and maintaining the security principles implemented in the browser engines.

As described previously, Wasm is already more efficient than technologies based on JavaScript, the performances are currently still quite low compared to a native application in C/C++.

# Software compatibility

Project compatibility will depend on the user's OS. The goal is to be compatible with Windows, Linux, Mac.
It will be necessary to be able to compile a version for each of these OS, which is dependent on the platforms supported by Electron.

The following platforms are supported by Electron:

## MacOS

Only 64bit binaries are provided for macOS, and the minimum supported macOS version is macOS 10.11 (El Capitan).

Native support for Apple Silicon (arm64) devices was added in Electron 11.0.0.

## Windows

Windows 7 and later are supported, older operating systems are not supported (and will not work).

Both ia32 (x86) and x64 (amd64) binaries are provided for Windows. Native support for Windows on Arm (arm64) devices was added in Electron 6.0.8. Running apps packaged with previous versions is possible using the ia32 binary.

## Linux distros

The prebuilt Electron binaries are built on Ubuntu 18.04.

Whether or not a pre-compiled binary can run on a distribution depends on whether or not the distribution includes the libraries that Electron uses to compile the application. As a result, only Ubuntu 18.04 is guaranteed to work, along with the following distributions to run Electron's pre-compiled binaries:

    Ubuntu 14.04 and above
    Fedora 24 and higher
    Debian 8 and higher

# Usage

## Compile VLC.js

_By following https://code.videolan.org/-/snippets/1343, and more particularly the build.md and step 0 to understand denv_.

In a directory of your choice (noted **DIR**)

```
git clone https://gitlab.com/garfvl/denv.git
```

<details>
<summary>Optional: Add Denv to the PATH</summary>

_https://askubuntu.com/questions/60218/how-to-add-a-directory-to-the-path_
_Reminder : **DIR** here is the directory from which you cloned denv_


In ~/.bashrc, add (last line)


> export PATH="**DIR**/denv/bin:$PATH"

```
source ~/.bashrc
```
</details>

```
git clone https://code.videolan.org/jbk/vlc.js --branch incoming
cd vlc.js
```

If the PATH is set for denv: 
```
denv compile-vlc-wasm bash compile.sh
```

Otherwise
```
./PATH_TO_DENV/denv compile-vlc-wasm bash compile.sh
```

# Run the demo

To setup namespaces with Docker:

```
chmod -R 777 vlc/
chmod -R 777 emsdk/
chmod 777 experimental.*
```

Then 

```
source emsdk/emsdk_env.sh
```

A reboot here is necessary.

```
emrun --no_browser vlc.html
```

And finally, go to http://localhost:6931/vlc.html

## Integrate VLC.js with Electron
First of all, npm and Node.JS must be installed and VLC.s compiled in your Electron folder.

```
mkdir electron-vlc
cd electron-vlc
npm init
```

The package.json file should look like this:

```
{
  "name": "electron-vlc",
  "version": "1.0.0",
  "description": "VLC.js in Electron",
  "main": "main.js",
  "scripts": {
    "start": "electron ."
  },
  "author": "Erwan",
  "license": "ISC"
}
```

Add Electron to the project.

```
npm install --save-dev electron
```

Add the main.js file which will contain:

```
const { app, BrowserWindow } = require('electron')
const path = require('path')
const Console = require("console");

function createWindow () {
    const win = new BrowserWindow({
        width: 800,
        height: 600,
    })
    win.loadFile('vlc.js/vlc.html')
    //win.loadURL('http://localhost:6931/vlc.html')
}

app.whenReady().then(() => {
    createWindow()
})
app.commandLine.appendSwitch('enable-features', "SharedArrayBuffer")
app.on('window-all-closed', function () {
    if (process.platform !== 'darwin') app.quit()
})
```

```
npm install
```

```
npm start
```
