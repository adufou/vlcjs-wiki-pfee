# Project outcome

The VLC.js project proposes to create both a product and a service around VLC in browsers.

### The VLC.js product

VLC.js will first be a product, based on an open-source project. The final deliverable will be an in-browser media player (does not require an additional app):

- Compatible with all major browsers (Google Chrome, Apple Safari, Mozilla Firefox, Microsoft Edge).
- Based on WebAssembly technology (also called Wasm) which aims for an execution speed close to native formats, therefore perfectly suited to multimedia, which consumes a lot of resources and hardly supports performance differences.
- Modular, with a dynamic module loading system to download only what is necessary and sufficient for playing a media
- Integrating all the functionalities of a "standard" VLC player for office automation.
- Open-source (the code will be released under the LGPLv2 license).

### The VLC.js Electron product

The objective will be to take VLC.js and integrate it into the Electron framework. Electron is a framework for building desktop applications using JavaScript, HTML, and CSS. Here are the contributions:

- Compatible with a multitude of operating systems (MacOS, Windows, Linux distributions).
- Compatibility linked to the operating system and no longer to the browser which is Chromium for all.

### The VLC.js service

The project also plans to set up a VLC.js publishing service:

- It will make available the VLC.js product mentioned above.
- It will also make available all the modules revolving around the basic player.
- The service will be free.

This service will allow any web developer to easily integrate the VLC.js product into their activities.


# The contribution of the VLC.js project

Videolabs started from feedback from the VLC community, both professionals and the general public, with a unanimous observation: the multimedia players integrated by default in browsers are functional, but particularly limited.

They are designed to work with particularly restricted video formats, from publication platforms such as Youtube or Dailymotion.

### The need of professionals

VLC is widely used in the world of audiovisual, broadcast and streaming, mainly as a tool for verifying the relevance and quality of media acquired or produced. It is very often the natural reader of DTT or satellite streams without transformation.

However, browsers have a particularly limited reader in terms of format & codec support (no 4:2:2 or 10-bit color encoding, no native MPEG2 TS format, etc.). In addition, few controls are possible, especially in color correction, or video filters.

The problem is the same for audio support, especially for Next-Gen Audio technologies (Object-Based Audio, Ambisonics, many audio channels, high bitrates), as well as for professional audio formats, not supported by browsers.

This is the vast majority demand identified during Videolabs' participation in the largest streaming trade show (IBC 201834).

# Usage of the VLC.js project

### Interface

The interface developed initially is very simple and is not the objective of the project. This has a central element which is the reader:


The basic functionalities such as play, pause, sound and progress display are present.

### Version 1.0

The first version displays the media player, as above, in the Electron application and plays its content which will be predefined. 

### Version 2.0

The second version will take any type of input file and read it into the app.

### Version 3.0

The third version will change the way vlc is called, until now we used a main.c which called the functions needed to play the media. We will now have to use the functions of our main to use them as a library and call them once compiled in our javascript part. This method will make the vlcjs library public.
