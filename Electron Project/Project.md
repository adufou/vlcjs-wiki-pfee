# Perimeter
The objective of the project: to check the technical reliability of creating a multimedia player with the Electron framework
Create a demonstrator to show customers the capabilities of their library.


# Deliverables
Binary Electron cross-platform
recipe/docker-compose for build VLC.js, then build an Electron instance with artifacts from the VLC.js compile.


# Risks
- Infeasibility due to JS limitations
- Infeasibility due to Electron limitations
- Non compatibility of VLC.js with Electron
- VLC.js platform modules are experimental for Emscripten (-> for Electron car Web)

# Difficulties
- Bad delimitations of the project of the first part of the PFEE
- Change of approach
- Reformulation of the subject

# Trello

https://trello.com/b/xOP0uhat/demo-electron
