# Prerequisites

WSL 2 (!! The tutorial is based on that!!)

Install (Microsoft Store):

- Windows Terminal
- Ubuntu 20.04 LTS (<- For WSL 2)

If when using denv in WSL error:
> The command 'docker-compose' could not be found in this WSL 2 distro.
> We recommend to activate the WSL integration in Docker Desktop settings.

In Settings -> Resources -> WSL Integration -> Check the distro (Ubuntu 20.04 for example)

# Build

_Following https://code.videolan.org/-/snippets/1343, especially the build.md and step 0 to understand denv_

Launch WSL (In Windows Terminal, to the new tab button)

In a directory of your choice (noted **DIR**)

```
git clone https://gitlab.com/garfvl/denv.git
```

<details>
<summary>Optional: Add Denv to the PATH</summary>

_https://askubuntu.com/questions/60218/how-to-add-a-directory-to-the-path_
_Reminder: **DIR** here is the directory from which you cloned denv_

in ~/.bashrc add (last line)

> export PATH="**DIR**/denv/bin:$PATH"

```
source ~/.bashrc
```
</details>

```
git clone https://code.videolan.org/jbk/vlc.js --branch incoming
cd vlc.js
```

IF the PATH is set for denv:
```
denv compile-vlc-wasm bash compile.sh
```

OTHERWISE
```
./PATH_TO_DENV/denv compile-vlc-wasm bash compile.sh
```

# Run the demo

If you have not setup the namespaces with Docker: (Recursive and there are LOTS of files)

```
chmod -R 777 vlc/
chmod -R 777 emsdk/
chmod 777 experimental.*
```

Next

```
source emsdk/emsdk_env.sh
```

It didn't work for me the first time, and while testing stuff I rebooted my WSL (I think, to check tell me when you're there) and the source was working

```
emrun --no_browser vlc.html
```

go to http://localhost:6931/vlc.html
