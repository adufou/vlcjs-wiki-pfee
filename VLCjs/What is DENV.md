Denv is a wrapper on "Docker run" to run with the right parameters the right image.

denv will retrieve the last tag available from the image, and will execute the command "docker run -v..." with the correct folder

To get the details of the denv command, do "bash -x denv"

When you build an image, you have to tag it to use it more easily

At VLC the images are tagged by date, we don't need to build them normally.
